# ARCoaching

Bienvenue sur le dépôt GitHub du site de coaching d'Alexandre Rouly ! Ce site est conçu avec le framework Symfony en PHP et utilise MySQL comme base de données. Il offre des programmes de coaching personnalisés pour aider les utilisateurs à atteindre leurs objectifs de fitness et de bien-être.
